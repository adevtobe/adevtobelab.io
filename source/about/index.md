---
title: About Me
date: 2020-08-03 22:40:03
---

I\'ve been fascinated by the magic of coding ever since I first painstakingly created an animation in HyperCard in an \"Introduction To Programming\" class in high school. That got me hooked and knew I wanted to do something with computers in college. After taking multiple programming classes I was a few credits away from getting an associates degree in programming when I began to contemplate the next steps. I was unsure what the job prospects would be and while I loved programming, someone once told me never to make your passion your job because it would take all of the fun out of it and I took that advice to heart. I ended up switching majors to accounting and transferred to a four-year university where I got my bachelors and masters.

After graduating, I was fortunate to get a job in public accounting where I worked for over seven years. While I enjoyed my time as an accountant, I missed all the little things unique to programming. Things like the excitement of exploring a new technology, the elation at solving a stubborn bug, the deep focus of brainstorming a solution to a challenging problem, and the pride and satisfaction in creating something unique. Ultimately I realized that if I didn\'t give myself the chance to explore software engineering as a career, it would be something that I would regret for the rest of my life.

A Dev to Be is meant to be the documentation from my switch to my current career to software development and also serve as a way for me internalize what I learn. As a professor once told me, \"The best way to make sure you understand something is to try and explain it to someone else. If you can\'t do that, you\'ll quickly find out where your knowledge gaps are.\"

I honestly don\'t expect anyone to come across this blog organically but if you somehow did, thanks for stopping by, and I hope you find it useful.
