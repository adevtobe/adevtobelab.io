---
title: How To Create a "Hello World" API Route in Ruby on Rails 6.1
date: 2021-11-12 22:21:15
tags:
---

### How To Create a "Hello World" API Route in Ruby on Rails 6.1

When learning a new language, a traditional first program to write is one that simply prints "Hello World" to the console/screen. In this series of posts, I'm going to explain how to do that in Ruby on Rails through a few different methods.

In this post, I'm going to walk through a way to do this using just the Rails API. Let's get started!

### Installation

First, you're going to need to install everything required to code in Ruby on Rails on your system. Since giving instructions for that would take a blog post on their own, I'm going to instead just provide the link I used to set up my system. It provides instructions for all operating systems so be sure to select the correct one:

https://gorails.com/setup/ubuntu/20.04

Good luck!

### Creating a Project

Once you have everything installed, open up a terminal window your favorite IDE and navigate to the directory you want to create your project in.

Once there we're going to create a new Rails app, but since we are building an app that will be an APIR server above all else, we don't want the full-blown app, so we need to make sure to use `--api` flag which will do three main things:

1. Only configure our app to use a limited set of middleware
2. Make `ApplicationController` inherit from `ActionController::API` instead of `ActionController::Base`
3. Configure the generators to skip generating views, helpers, and assets whenever we generate a new resource

So let's run the code:

```ruby
rails new hello_word --api
```

### Running the Server

You should now have a directory called "hello_world" with a bunch of other directories that are required for your app to work. To make sure everything is working as is, before we proceed let's start the server that is included and see if we can see the home page.

In your terminal, enter the following command:

```ruby
rails server
```

Then, open up a browser and navigate to `localhost:3000`. You should see the Rails home page:

![](images/ruby_on_rails_homepage.jpg)

### Creating a Controller

Next we will need to create a controller, which is essentially a class with methods that we can refer to in our routes. These controllers are located in `.rb` files in the `/app/controllers` file. If you navigate there, you'll see there is already one file, called `application_controller.rb`.

We're going to leave that file alone though and create one of our own. To do so, just run the following command.

```ruby
rails g controller hello
```

The `g` flag stands for generate and `hello` is the name of the controller. This will create a file called `hello_controller.rb` in the controller folder.

Once this has run, we're going to define a method in the new controller to actually render the message "Hello World!" in our browser when the route gets hit.

To do this, we're going to render out some HTML so navigate to the `hello_controller.rb` file and enter the following:

```ruby
    # /app/controllers/hello_controller.rb

    class HelloController < ActionController
    	def world
    		render html: '<h1>Hello World!</h1>'.html_safe
    	end
    end
```

### Creating a Route

Now that we have a controller, we need to create a route to point to it.

First, open the `/app/config/routes.rb` file since this is the file where we define all of our routes. Next, let's say we want to render the above method when someone goes to the `/hello` route so enter the following line of code:

```ruby
Rails.application.routes.draw do

	get '/hello', to: 'hello#world'

end
```

Now, when our Rails application receives an incoming request for `GET /hello`, that request will be dispatched to the `hello` controller's `world` action.

Before we try it out, we can confirm if the route was successfully added by running `rails routes` and looking at the first entry:

![](images/rails_routes.jpg)

Now, the moment of truth. If your server is running, let's get it started with the `rails server` command. Then, open up a browser and go to http://localhost:3000/hello. Your page should look similar to the below:

![](images/hello_world.jpg)

And like that, we have written a hello world page using only a Rails API route!

In the next post, I'm going to kick things up a notch by explaining how to do this using a React front-end. See you then!
