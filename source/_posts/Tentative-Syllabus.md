---
title: My Tentative Syllabus
date: 2020-08-20 22:44:48
tags:
---
I\'ve had some stuff going on this past week and really haven\'t had time to code given they were higher priority and time\-sensitive however I thought I thought I\'d share my \"syllabus\" going forward.

As I mentioned earlier, while I have read conflicting information regarding whether having a CSS\/HTML foundation is absolutely necessary, I just want to learn so I figured I\'d start my journey there.

Back when I was still deciding what language to learn first, I had taken a beginner\'s Python course on Udemy by Colt Steele and really liked his teaching style. Because of this, I\'ve been keeping an eye out for any of his free material and noticed that he had a free [4 day Code Camp](https://invidious.13ad.de/playlist?list=PLblA84xge2_xNtaFnZhefjFbnDrpySKD3) that introduced beginners to HTML and CSS so I figured that would be a great place to get my feet wet, so to speak.

I\'m currently on the Day 2 of the video and afterwards I\'m thinking of taking Fullstack Academy\'s free bootcamp prep course which I believe is a much more comprehensive dive into HTML and CSS as well as the beginnings of Javascript. 

See you next week!