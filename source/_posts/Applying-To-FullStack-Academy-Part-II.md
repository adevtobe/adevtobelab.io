---
title: Applying-To-FullStack-Academy-Part-II
date: 2021-02-23 23:21:11
tags:
---


#### A Quick Update ####

I\'m noticing that there\'s a trend happening where I often go a month or longer without posting and it looks like I broke my previous record by it being two months since my last post.

The reason for this is I ended up passing the interview and got accepted into Fullstack! It has been exactly as advertised in that sucks up every second of free time you have available, forcing you to prioritize everything and unfortunately this blog has not made the cut. I have drafts of various posts written up but never had time to finish them. Having now made it to the mid-point though, this week is a bit of a \"breather\" week where we don\'t have any lectures and instead are encouraged to study up on any weak points and also explore new technologies not covered in the course yet.

Because of this, I thought I\'d devote some time to getting this blog back on track and to kick it off I thought I\'d dust off the oldest draft and wrap up my last post.

#### The Interview ####
As mentioned in this great article, [How to Ace the Fullstack Academy Interview](https://www.fullstackacademy.com/blog/how-to-ace-the-fullstack-academy-interview), after the coding assessment there is what I would categorize as a behavioral interview.

If you\'ve ever been in a job interview, you\'d find this very similar and can expect the questions to be in the same vein, asking you to give specific examples that illustrate you have the skills and experience to advance to the bootcamp. While I don\'t believe I can give the exact questions I asked, I would suggest looking up the STAR interview response method and trying to use that model to help your develop your responses. If you do that, you will be more than prepared for anything that comes up.

My experience ended up being great and I got along with my interviewer really well. I ended the Zoom call feeling that I had left nothing on the table and felt great about my chances of being accepted. Luckily I didn\'t have to wait long to find out and the next day, December 3 I received an email saying I had been accepted and outlining the next steps.

#### Going Forward ####

The Foundations phase, were you really get down the fundamentals of JavaScript, started on December 5th and since then it\'s just been non-stop learning. After Foundations we moved to the Junior phase, learning the backend (PostgresSQL, Express, Sequelize, and Node.js) and after that we learned the front end (React and Redux). As mentioned above, next week we will be moving on to the Senior phase which consists of pretty much using everything we learned to build web apps.

Going forward, my posts might be a bit random as I think the best way to keep this blog active is for me to talk about whatever is on my mind rather than stick to a path. The reason for this is that, when I promise that I will talk about a certain topic in my next post, if I\'m not able to write that but have enough notes to put together a post on a different topic, I\'ll hold off since I feel I need to stay committed to what I said previously.

Hopefully this will result in less time between posts going forward!
