---
title: Bootcamp here I come?
date: 2020-11-09 21:53:44
tags:
---

So, as you can probably tell by the date of this post compared to my last post, it\'s been almost two months since I\'ve made an entry. Well, it\'s been a crazy two months.

I pretty much got so swamped with work, logging in way more than 40 hours a week almost all the way through October, that I had no time to code. As the days went by, I realized that my rough plan of taking some classes in my free time and gradually working up to an employable level of knowledge would not work. My job tends to be seasonal and there is no way I could go two months at a time, multiple times a year, not coding at all and expecting to progress.

I realized that I needed to really just make a decision. Did I really want to be a software engineer? If so, the only way I would be able to achieve that is by leaving my job and focusing 100% on programming.

There\'s a few problems with this though:

* I am the sole provider for my family
* We have limited savings to last on if I\'m not working
* We\'re still in the middle of a pandemic so it seems absolutely nuts to quit a well-paying job at this time, especially given the above two points

Still, as I may have mentioned in a earlier post, I\'ve played it safe my whole life without taking any real risks. Let\'s say I do quit my job to learn how to code and the worst case happens and I am unable find a job as a junior dev. Will I be able to find another job in my current profession? It might be difficult, and I might not make the same, but given my experience I think that is very likely.

So in the end, I might be out some money, but I could at least say I tried and not live my life with the regret of having not done so. Because of that, after talking it over with my spouse, we decided we were going to try.

Having already done research on coding bootcamps, I knew I wanted to go with FullStack Academy. That very night we made the decision I signed up for their Bootcamp Prep course; and I have to say, paying for that was a bit exciting as it felt like I was taking the first real step to a new career.

I then signed up for their free Intro to Coding course, which covers the very basics of HTML, CSS, and Javascript, and is intended for complete beginners to get them ready for the Bootcamp Prep course. Even though I have a small amount of experience with HTML and CSS, Javascript is new to me so I thought it would be a good fit.

After finishing the course, and the pre-work for Bootcamp Prep, I still had no doubts this was the right decision as I found everything just as fun and exciting as I have in the past.

Right now, I\'m on my second week of Bootcamp Prep and it just feels *right*. The plan is then to apply, and if I get in, still work full-time while taking the first month of bootcamp which is supposedly manageable with only a 15-30 hour a week workload. The subsequent 3 months though will be intensive and the plan is to quit my job and focus 100% coding.

So in the end, my goal is to be a junior dev somewhere by the end of April 2021 and I plan to do everything I can to make that happen.

Part of \"everything I can\" is to continue learning in public which means continuing this blog. My idea is to give summaries of what I learn going forward along with my general thoughts and takes on the whole process of switching careers.

See you in the next post!