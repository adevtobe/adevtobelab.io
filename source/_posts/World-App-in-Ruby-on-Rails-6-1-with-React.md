---
title: How To Create a "Hello World" App in Ruby on Rails 6.1 with React
date: 2021-11-15 22:12:16
tags:
---

Sometimes you may want to use Rails exclusively for the back-end of your app and React for the front-end. In this next post in our series of different ways to create a "Hello World" app in Rails, we're going to show you how to set that up.

While there are a few different approaches to integrating React with a Rails app, we're going to assume that we're going to use Webpack in this app so we're going to take the approach that installs React and integrates it with Rails all in one go.

### Creating a Project

To create a new Rails app withe Webpack already pre-configured for React, we can run this command:

```ruby
rails new react_on_rails --webpack=react
```

After this has finished (it may take a few minutes) cd into the new directory and open it up in your favorite IDE.

Once there, you will see the directories and files in the file explorer look pretty similar to standard layout when using the `new` command. If you expand the app directory though, you'll see that there is now a `javascript` folder. This where all files related to React will be located.

Specifically, if you look in the `/javascript/packs/` folder you will see a file called `hello_react.jsx`. This is the component of our Hello World app that comes as boilerplate however if run the Rails server, you will see this is not being served and instead we get the normal landing page. Let's change that.

### Create a Controller

Similar to how we did it in the previous post, we can create a controller using the `generate` command however we're going to pass an additional parameter to the command after the name.

```
ruby rails g controller pages index
```

What the additional parameter of `index` does is create an empty method of the same name in the pages controller file.

After the command is run, if you open the pages controller you should see the following:

```ruby
class PagesController < ApplicationController
	def index
	end
end
```

### Create a Route to Root

Next, if we go to `/config/routes.rb` we can confirm that it also created a route to the controller. Since we want to see "Hello World!" instead of the landing page, let's change that to the following:

```ruby
Rails.application.routes.draw do
	root 'pages#index'
end
```

Now when someone navigates to root it will run the `index` method in our controller which will then serve the `index` view. If you open this file (`index.html.erb`) you will see it just has some placeholder text:

```html
<h1>Pages#index</h1>
<p>Find me in app/views/pages/index.html.erb</p>
```

If you start the server (`rails server`) and visit the root page, that is exactly what you will see:

![](images/default_index_page.jpg)

To see the React component from above, we're going to need to inject it into this view.

### Inject React into Rails

In order to do this, we need to first go back to the `hello_react.jsx` file. At the top, we we some comments describing exactly how to do this using the pack tag:

```html
<%= javascript_pack_tag 'hello_react' %>
```

So let's copy that and replace the html in our `index.html.erb` file with it. Now, if we reload the home page, you should see the text "Hello React".

This post isn't about how to create a 'Hello React' app though so let's go to `hello_react.jsx` file and modify it so it looks like the following:

```javascript
import React from 'react';
import ReactDOM from 'react-dom';

document.addEventListener('DOMContentLoaded', () => {
  ReactDOM.render(
    <div>
      <h1>Hello World!</h1>
    </div>,
    document.body.appendChild(document.createElement('div'))
  );
});
```

And there you have it!

![](images/hello_world_react.jpg)

In the third in final post in this series, we're going to create our Hello World app using the "standard" Rails approach.

See you then!
