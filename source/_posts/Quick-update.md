---
title: Quick Update
date: 2020-09-04 22:13:10
tags:
---

A little late with this post and don\'t really have time to make it long so it will just be quick.

As it\'s been a little while since I\'ve been able to write or code, already some things were getting rusty so I had to search for how to do a few things.

Given I only have a few minutes today, I thought I would just do some maintenance, mainly check if there were any updates to Hexo. How do I do that again? Good thing I [wrote a post about it](https://adevto.be/2020/08/06/upgradehexo/). This learning in public thing is coming in handy.

Before I ran the update command in npm, I was curious if there was any way to see first what needed to be updated. After finding some packages I could install, I saw that there was something already baked in:

``` console
npm outdated
```

This gives a summary of the packages that need to be updated which resulted in the following:

![](images/npm_outdated-1.jpg)

To quote [Steve1989MREInfo](https://invidious.13ad.de/channel/UC2I6Et1JkidnnbWgJFiMeHA), \"Nice.\"

This showed that there were some updates to be applied. I then ran the command I had discovered in my previous post:

``` console
npm update
```

It did its thing and after it was complete, I ran \"npm outdated\" again:

![](images/npm_outdated-2.jpg)

Hmm . . . that\'s strange. It updated Hexo and the theme I use but nothing else. I know I previously read about the differences between the \"Current\", \"Wanted\", and \"Latest\" columns but that was now a couple of weeks ago so it\'s time for a refresher to figure this out. Me thinks the color of the text is a clue but unfortunately I\'ve run out of time.

Guess I know what my next post will be about. :)

