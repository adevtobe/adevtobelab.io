---
title: Applying To FullStack Academy in 2020
date: 2020-12-01 20:39:55
tags:
---
As I mentioned in my previous blog post, I decided on [Fullstack Academy](https://www.fullstackacademy.com/) as the coding bootcamp I wanted to attend. Since that post I have completed the Bootcamp Prep course, completed the coding assessment, and am now preparing for the interview which I have later this week. During this process I came across some things that I just thought I\'d share in case someone else is considering doing the same down the line.

### How to Apply to Fullstack Academy - Part I ###

So let\'s say you\'ve decided to apply Fullstack Academy. What do you have to prepare for?

#### The Coding Assessment ####

Well first, you will have to take a coding assessment. This may seem strange to some since many would think, \"Wait, I\'m signing up to learn JavaScript but they expect me to know some first?\" Well, yeah, pretty much. I know there are other bootcamps out there that have no expectations of any prior coding knowledge, but I found a somewhat common theme when researching those websites. When I\'d come across a review on Reddit, YouTube, a blog, etc. and the reviewer comments on what they wish they would have done prior to signing up to increase their chances of success, they often said something like, \"I wish I would have gotten the basics down before applying.\"

The reasoning for this is, as I\'m sure you\'ve come across many times, people equate a coding bootcamp to trying to sip water shooting out of a firehouse. There is just so much information to process and it\'s moving so fast that it\'s difficult to absorb.

For example, you can be pretty sure that you will be using an IDE in your bootcamp, probably [Visual Studio Code](https://code.visualstudio.com/) since it\'s one of the more popular ones right now. If you've never had any experience with it before, and then one day of camp you\'re expected to install it and start using it efficiently, if any issue comes up you\'re going to quickly fall behind since the next day you\'ll be dealing with something different with it\'s own issues.

Because of this, I would wholeheartedly recommend doing as much as you can to learn the basics so that when you get to the real meat and potatoes of the course, you\'re not spinning your wheels from the start and can dedicate your time to focusing on the more important topics.

I went off on a bit of a tangent there, so to get back on track, yes, you need to have the basics down before you take the assessment. Good news, if you sign up for Fullstack\'s Bootcamp Prep course, it will teach you everything you need to know. My one big piece of advice on this though is, assume anything learned in that course will be fair game. You may get some advice of \"focus on this\" and \"don\'t focus on that\" but I would recommend not listening to that. Study everything you learned equally and get comfortable with it.

Finally, in regards to my own piece of \"I wish I would have done this\" word of caution is that once you apply, you will receive an email stating you have 48 hours to complete your assessment. I could have missed this but nowhere did I come across this tidbit. This is extremely important because if you are planning on studying more before taking the assessment, surprise, you only have 48 hours. Or, in my case, if you apply on a Sunday night thinking you can take the assessment at any time, surprise, you\'re going to have to take it on a Wednesday.

#### To Be Continued ####

Well, it turns out I had more to write about than I thought and it\'s getting late so I guess this will be a two-parter.

See you at the next post!


