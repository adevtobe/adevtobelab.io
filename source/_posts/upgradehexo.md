---
title: How to Upgrade Hexo to Version 5.0
date: 2020-08-06 21:50:49
tags:
---
So I thought a lot about what my inaugural post would be on this blog while I was figuring out how to get it running.

Would it be about why I\'m so excited to start down the road to becoming a software engineer? How I decided that Javascript would be the first language I would learn? How I chose this particular static site generator? 

Eh, not exactly. While those would all be great, I think the most important for me to do was actually just publish *something*. All the above ideas would take a while for me to write since I have not yet learned how to turn off the perfectionist side of me and my free time is too rare to spend it that way.

So what\'s it going to be . . . how to upgrade Hexo!

Seriously, being completely new to static generators, the first issue I ran in to was the fact that I couldn\'t get the alternate theme config file to work for this theme. Based on [Hexo\'s docs](https://hexo.io/docs/configuration.html#Using-an-Alternate-Config), you should be able to create a separate config file in the root folder but no matter what I did, none of the changes I made there would flow to my theme.

I must have re-read that page a million times and was 100% certain I was doing everything correctly. Why wasn\'t it working? The only requirement was that I needed to be running the latest version of Hexo but I had literally installed it 4 days before so that wasn\'t the issue.

I hit DuckDuckGo again but the pickings were slim. Begrudgingly I opened a tab in Firefox with Google, reminding myself that I would need to remind [BleachBit](https://www.bleachbit.org/) before I shut down for the day, and tried looking there. I found a couple more leads but nothing panned out and I was still stuck.

So, like I had done countless times before, I went back to beginning. What was I missing? At this point, given the level of exasperation I was feeling, I new it had to be something trivial. Irony has a way of rearing its head in times like this. Again, I read the docs. Again I saw the only requirement was that I had to be running the latest version of Hexo. And I again I reassured myself that wasn\'t the issue since I had just installed it 4 days ago. There was no way they could have released a major update within that timeframe. The odds would just be incredible . . . and it would be the exact trivial thing that would make this whole thing laughable.

Still being new to Hexo, I quickly searched for the right command. I then went to my command console, navigated to the root of my blog, and ran the following code:

``` console
hexo version
```

The output . . . 4.2.1!

Son of a %#@#!

Of course they had released an update in the 4 days I was playing around with it.

Rejuvenated, I quickly went through the Hexo docs again. Okay, how do I update this thing. I ended up finding the answer in the [News section](https://hexo.io/news/2020/07/29/hexo-5-released/) (or so I thought) where it said:

"To upgrade to Hexo v5, change the following line in your package.json,

``` javascript
package.json

- "hexo": "^4.2.1",
+ "hexo": "^5.0.0",
```

Okay, I can do that. I made the change and then restarted the server. Still nothing. My changes to the config file weren\'t flowing. I ran a version check and saw that I was still on 4.2.1. More searches. More translating websites from Chinese to English. But I could smell blood in the water, I knew I was close. 

Then I found it.

### How to Upgrade Hexo from Version 4.2.1 to 5.0.0

So here\'s what you need to do:

Open up package.json which is in the root of your blog folder and make the below change:

``` javascript
package.json

- "hexo": "^4.2.1",
+ "hexo": "^5.0.0",
```

Then go to your console and run the following command:

``` console
npm update
```

And that\'s it. You are now the proud owner of Hexo Version 5.0.

Anti-climactic? Sure, especially if you aren\'t a complete beginner to Javascript. For me though, it was my first battle, and I was victorious. I hadn\'t given up and asked for help and had powered my through it. As soon as I saw the changes in my alternate config file flow to my blog I raised my fists up in the air in triumph and gave a muffled \"Yeah!\" because by this time it was almost 11:30pm and I did not want to wake up my son who was sleeping in the room directly above the one I was in.

But really, the reason I thought this mundane issue was post-worthy was because I absolutely loved the whole process. Sure I was frustrated at some points but that is just part of the game. And to me, this is why I want to give software development a shot. This is what attracts me to coding; facing a problem and needing the patience and persistence to work out a solution. I\'m sure plenty of people can do that but I doubt many people enjoy it. I just happen to be one of the people that does.

