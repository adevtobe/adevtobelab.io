---
title: Tracking Coding Time
date: 2020-08-13 21:50:17
tags:
---
Now that I\'ve got the blog set up, the next step is to obviously start coding! But before I do, there\'s one more tool I needed to grab.

While I haven\'t yet made a post about what my plan is for becoming proficient enough to land a junior developer job, I\'ll drop a spoiler and say I am considering signing up for a bootcamp eventually. The thing with bootcamps is you need to make sure you are able to dedicate enough time to each week to meet their requirements otherwise it will just be money down the drain.

This part is tricky since I am currently working a full-time job so I don\'t exactly have a huge block of time during the week to dedicate to coding. Because of this, I need to get a realistic estimate of how much time I can dedicate regularly to coding. It\'s one thing to just say, \"I will code X hours a day\" but who knows if that will be achievable in practice.

So, I decided this week that I needed to find a way to track my time spent on this journey. I thought at first I would just crank out an Excel spreadsheet but I wanted to see if there were more elegant, ready-made solutions out there. I did a quick search on DuckDuckGo for \"open source time tracker\" and the first hit was something called [Kimai](https://www.kimai.org/). I went to their website, poked around a bit, and was surprised to see it checked all the boxes of what I wanted to do and even better, it was free!

Wanting to do my due diligence though, I spent 10 minutes or so looking at other options but ultimately came back to Kimai. It required that I run a local server but personally, I prefer that option to having something stored in the cloud, even if it is just my timesheets.

In the past, I\'ve used [XAMPP](https://www.apachefriends.org/index.html) for running local servers but in this case, I saw that there was another software called [AMPPS](https://www.ampps.com/) that was created by Softaculous which actually came with Kimai baked into it.

After watching and reading a few tutorials, I downloaded and installed AMPPS then Kimai and started setting it up.

The way Kimai works, based on my quick reading, is you can create a Customer who can have one or more Projects associated with them and each Project can have one more Activities. After ruminating a bit, I figured I would just have one Project which is \"Learning to Code\" and for now, the activities I would want to track would be the following:

- Blog
- HTML/CSS
- Javascript

Oh yeah, surprise, I decided I am going to learn Javascript as my first language and am thinking I should learn some HTML/CSS as well though the jury is still out on that (I\'ve read conflicting opinions of whether that is still necessary). I definitely plan on writing a post later explaining my rationale behind this but I really am antsy to get to coding so I\'m not sure when I\'ll get around to that.

So, after setting up Kimai here is how my first few timesheet entries look like since I started using it:

![](/images/Kimai_Time_Tracker.jpg)

The dates and times are not exactly correct and I think it\'s because I did not set up the time zone correctly but I can address that later. I really only care about the durations. As you can see, as of now I\'ve spent almost an hour in this session. Pretty snazzy stuff.

Well, that\'s it for this post. I now have everything I need to start coding and am super excited to start. I don\'t know if it\'s just pure optimism but I really think that this is something I can do and I can just see myself starting as a junior developer some time next year. Let\'s go!