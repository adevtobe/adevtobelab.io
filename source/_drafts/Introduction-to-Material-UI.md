---
title: Introduction to Material UI
tags:
---

### An Introduction to Material UI

While I've learned a lot in this first half of bootcamp, one area that I know I still need to work on is design. While I feel that I can tell a well-designed website from a poorly-designed website when I visit one, I'm just not at the point where I can come up with that a design on my own.

I think a lot others are in the same position and believe this is just something that eventually comes with practice. Still, given the pace of the courses, we need something to show off our fancy components and back-end logic in the meantime.

Wouldn't it be great if there was library, framework, etc. that took care of a lot of the designing with minimal effort on our part? Something that with just a few extra lines of code could take our Times New Roman front-end and turn it into something pleasing to look at? Well, allow me to introduce you to Material UI.

#### What is Material UI?

Material UI is one of the most popular component libraries currently being developed, averaging around 30,000 downloads a week as of the time of this writing.
