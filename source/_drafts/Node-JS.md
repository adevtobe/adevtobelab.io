---
title: Node-JS
tags:
---

### All about Node ###

If you're thinking of learning in JavaScript, you will mostly like need to install and use Node.JS. But what is it? And how do we use it?

#### What is Node.js? ####

To quote this [SIMFORM article](https://www.simform.com/what-is-node-js/):

>Node.js is an open-source, Javascript runtime environment on Chrome’s V8 that lets you effortlessly develop fast and scalable web applications. It utilizes an event-driven, non-blocking I/O model that makes it lightweight, efficient and excellent for data-intensive real-time applications that run across shared devices.

Well that clears up things, doesn't it? Let me try and put this in layman's returns, but please bear in mind that I'm still learning this. ;)

Let's look at the first sentence, specifically the part that says "runtime environment". What's a runtime environment? That is just the hardware/software environment in which a program is executed. In this case, this is environment that all the cool stuff we code JavaScript is run in. To illustrate this, let's say we have a file called `superamazing.js`. If we go into our terminal in VS Code and just type:

``` javascript
superamazing.js
```
After pressing enter you'll just get the following

``` javascript
superamazing.js: command not found
```
Why does that happen? Well, JavaScript was originally created to add functionality to browsers but here we're not running it in a browser so it just won't do anything. However, if you have have Node.JS installed and type the following:

``` javascript
node superamazing.js
```
Now your code will run because we're telling Node, "Hey, take this file and run it". That's what the above sentence means by runtime environment. It's a place we can run JavaScript files outside of the browser.

Let's move on to the next bit that says, "It utilizes an event-driven, non-blocking I/O model". While there's only 7 words in this phrase, explaining what this means would take a while so I'll just give a summary and expand on it in a later post.

Essentially, if you have multiple events you want to run, Node.js is able to handle them asynchronously (all at once) rather than just processing them one at a time.

#### How do we use Node.js? Importing and exporting explained ####

Okay, so Node.js allows us to run JavaScript files outside of the browser. How do we use it?

So the cool thing about node is that it has a lot of built-in modules that we can use. Think of a module as a file containing functions that exists outside of the file you're working in. Now, if you had a function that was in your working file, you would just invoke it. With modules though, we need to specifically tell our file where to find this file that has the functions/methods. How you do this, is with the `require()` function.

So let's we want to use the functions/methods inside a built-in module called "cool-module" that is included with node. At the beginning of our working JavaScript file, let's call it `app.js`, we would write the following line:

``` javascript
require('cool-module')
```
Notice how this is isn't in the form of a path nor does it have a file extension. The reasons for this are a bit complex so I'm just going to copy and paste this little blurb from the [Node.js website](https://nodejs.org/en/knowledge/getting-started/what-is-require/) which describes what it does:

>The rules of where require finds the files can be a little complex, but a simple rule of thumb is that if the file doesn't start with "./" or "/", then it is either considered a core module (and the local Node.js path is checked), or a dependency in the local node_modules folder. If the file starts with "./" it is considered a relative file to the file that called require. If the file starts with "/", it is considered an absolute path. NOTE: you can omit ".js" and require will automatically append it if needed. For more detailed information, see the [official docs](https://nodejs.org/docs/v0.4.2/api/modules.html#all_Together...)

Now, while Node's built-in modules can do a lot, what if you wanted to write your own module and use the functions in it in `app.js`? That's easy to do.

In VS Code, just create a separate file and call it whatever you want. Let's say it's called `hello.js` and include it in the same directory as `app.js`. In this new file, enter the following:

``` javascript
console.log('Hello');
```
To confirm this works, after saving, if you enter `node hello.js` into your terminal you should see `Hello` printed out.

Now go back to `app.js`. What if we wanted to print out `Hello` from here but use our `hello.js` file to do it? If you guessed we need to use the `require()` you're correct! Let's enter the following into `app.js`:

``` javascript
require(`./hello`)
```

Now enter `node app.js` into your terminal and you should see `Hello` printed out. That's because the require function will automatically run the module you specified. Because our module contained a console log, that's what it did.

But let's make it a little less simple. Let's go back to `hello.js`, delete out the console log, and replace it with this:

``` javascript
const hello = {
  greetingOne: 'Hello',
  greetingTwo: 'Hey there',
  greetingThree: 'Howdy'
};
```

Now if we run `app.js` again, you should see that `{}` is returned. That's because when `hello.js` was run the only thing that happened was that a variable was defined. However, when you require a module, it always wants to return something, And if you don't give it something to return, it will just return an empty object.

But say we wanted to export the`hello` object in our `app.js` file. How would we tell Node, "Hey! Send this object back!"? To do that, would need to add the following line to the bottom of the `hello.js` file:

``` javascript
module.exports = hello;
```
What this does is explicitly tell Node, "I know you want to return something, so return this object". Then in our `app.js` file, to use this object we would assign our require function to a variable at the top:

``` javascript
const hello = require('./hello');
```

####Returning Multiple Items From a Module####

Now let's say we had two objects in our `hello.js` file, such as the following:

``` javascript
const helloEnglish = {
  greetingOne: 'Hello',
  greetingTwo: 'Hey there',
  greetingThree: 'Howdy'
};

const helloSpanish = {
  greetingOne: 'Hola',
  greetingTwo: 'Buenas',
  greetingThree: 'Que mas'
};
```

How would we export these? We would do that by wrapping them in an object and exporting that, as shown below:

```javascript
module.exports = {
  helloEnglish,
  helloSpanish,
};
```
Now in our `app.js` file, we will have access to both objects and can use them as follows:

``` javascript
const hello = require('./hello');

console.log(hello.helloEnglish.greetingOne); // Logs out 'Hello'
console.log(hello.helloSpanish.greetingOne); // Logs out 'Hola'
```

#### Object Destructuring ####

But let's say we just wanted to import one object. Is there a way to do that? Indeed! Just change the require statement to the following:

``` javascript
const { helloEnglish } = require('./hello.js');
```

The above line will only import the key specified.

#### Conclusion ####

Hopefully now you have an understanding of what Node is and how we can use it to take advantage of built-in
