---
title: Working With Etebase
tags:
---

### Etebase: End-to-end encryption for developers ###

Some text.

#### Installing Etebase ####

To get started, we'll first need to install Etebase via NPM or Yarn. If you're working in Node.js you would run either of the following commands:

NPM
```javascript
npm install etebase
```
Yarn
```javascript
yarn add etebase
```

If you will be working with React-Native you would need to install the following:

NPM
```javascript
npm install etebase react-native-etebase react-native-get-random-values react-native-sodium
```
Yarn
```javascript
yarn add etebase react-native-etebase react-native-get-random-values react-native-sodium
```
Once you've installed it, you can now import it in your `index.js` file:
