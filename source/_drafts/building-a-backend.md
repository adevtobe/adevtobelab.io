---
title: Building a Backend
tags:
---

### A Brief Outline to Building a Backend ###

So let's say you have an idea for the next revolutionary todo app and you're itching to get started. Where do you start? Great question, I don't know if there's really an answer to that, but since I've already written the title to this post, let's start with the backend!

#### Some Assumptions ####

This post is going to assume you will be using the PERN tech stack which is compromised of the following:

- [Postgres](https://www.postgresql.org/)
- [Express](https://expressjs.com/)
- [React](https://reactjs.org/)
- [Node.js](https://nodejs.org/en/)

In addition, to manage our state and make it easier to talk to our database, we'll also be leveraging the following two libraries respectively:

- [Redux](https://redux.js.org/)
- [Sequelize](https://sequelize.org/)

Finally, we'll be assuming that you are starting from a blank directory.

#### Building a Simple Backend ####

Okay, so since we are going to be working exclusively on the backend we will probably need a folder to keep everything in so in your project folder, create a folder called "server":

``` javascript
mkdir server
```

Next, we will probably want a file to keep track of all the cool stuff we're going to install (our dependencies) so let's create that:

``` javascript
npm init -y
```

#### Creating a Server ####

Okay, next step is we're going to need to create a server to serve our files. We'll be using Express for that so we'll probably want to install that. And Express needs help talking to our database so we'll need to install node-postgres (pg) to do that. Think of it like Express and Postgres speak two different languages, and node-postgres is a collection of modules that serves as a translator. We can install both of these packages in one line:

``` javascript
npm install express pg
```

