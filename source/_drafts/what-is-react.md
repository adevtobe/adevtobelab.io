---
title: what-is-react
tags:
---

### All about React

``` javascript
import React from 'react';
import ReactDOM from 'react-dom';
```

``` javascript
class App extends React.Component {
  constructor() {
    super(); // Steals stuff from the parent
    // This allows us to use functions like this.setStatus()
    // Also allows us to set a "state" object
    // This is not modification, this is initialization (which is ok)
    this.state = {
      message: 'I really like burgers',
    }
  };
}
```

``` javascript
messWithState() {
  this.setState({
    message: 'I have now messed with the state',
  });
  // We want to force the "this" here to messWithState so we create a copy
  // of the function and bind it to messWithState
  this.messWithState = this.messWithState.bind(this);
}
```

``` javascript
render() {
  return (
    <div>
    <div>Hello from React!!</div>
    <p>{this.state.message}</p>
    <button onClick={this.messWithState}>
    Click here to mess with state
    </button>
    </div>
  );
}
```

``` javascript
ReactDom.render(<App />, document.getElementById('app'))
```
